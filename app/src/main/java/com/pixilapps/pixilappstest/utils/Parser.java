package com.pixilapps.pixilappstest.utils;

import com.google.gson.Gson;

public class Parser<T> {

    private Class<T> type;
    private Gson gson;

    public Parser(Class<T> type) {
        this.type = type;
        this.gson = new Gson();
    }

    public T parseFromJson(String jsonString) {
        T t;
        t = gson.fromJson(jsonString, type);

        return t;
    }

    public String parseToJson(T t) {
        String jsonString;
        jsonString = gson.toJson(t, type);

        return jsonString;
    }
}
