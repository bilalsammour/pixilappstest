package com.pixilapps.pixilappstest.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class Requester<T> {

    protected static final String TAG = Requester.class.getSimpleName();
    protected OnCallBackListener<T> onCallBackListener;
    private Context context;
    private Class<T> type;
    private String baseUrl;
    private String fullSuffix = "";
    private int timeout;
    private Map<String, Object> params = new HashMap<>();
    private Map<String, Object> headers = new HashMap<>();
    private AQuery aQuery;
    private AjaxCallback<String> callbackString = new AjaxCallback<String>() {

        @Override
        public void callback(String url, String result, AjaxStatus status) {
            Requester.this.processResult(result, status.getCode());
        }

    };

    public Requester(Context context, Class<T> type) {
        this.context = context;
        this.type = type;
        this.aQuery = new AQuery(context);
    }

    public Context getContext() {
        return context;
    }

    public Class<T> getType() {
        return type;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getFullSuffix() {
        return fullSuffix;
    }

    public void appendUrl(String suffix) {
        if (fullSuffix == null)
            fullSuffix = "";

        String separator;
        if (fullSuffix.endsWith("/") || suffix.startsWith("/"))
            separator = "";
        else
            separator = "/";

        fullSuffix += separator + suffix;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public Requester<T> addParameter(String key, Object value) {
        this.params.put(key, value);

        return this;
    }

    public Map<String, Object> getHeaders() {
        return headers;
    }

    public Requester<T> addHeader(String key, Object value) {
        this.headers.put(key, value);

        return this;
    }

    public OnCallBackListener<T> getOnCallBackListener() {
        return onCallBackListener;
    }

    public void setOnCallBackListener(OnCallBackListener<T> onCallBackListener) {
        this.onCallBackListener = onCallBackListener;
    }

    public void makeRequest(RequestType requestType) {
        makeRequest(requestType, null);
    }

    public void makeRequest(RequestType requestType, ProgressDialog dialog) {
        switch (requestType) {
            case GET:
                makeGetRequest(dialog);
                break;

            case POST:
                makePostRequest(dialog);
                break;
        }
    }

    private void makeGetRequest(ProgressDialog dialog) {
        String fullUrl = getFullUrl(true);

        checkHeaders(callbackString);

        aQuery.progress(dialog).ajax(fullUrl, String.class, callbackString);

        Log.i(TAG, fullUrl);
    }

    private void makePostRequest(ProgressDialog dialog) {
        String fullUrl = getFullUrl(false);

        checkHeaders(callbackString);

        aQuery.progress(dialog).ajax(fullUrl, params, String.class,
                callbackString);

        Log.i(TAG, fullUrl);
    }

    private String getFullUrl(boolean isWithParams) {
        String fullUrl = "";
        if (!TextUtils.isEmpty(baseUrl))
            fullUrl += baseUrl;

        if (!TextUtils.isEmpty(fullSuffix)) {
            String separator;
            if (fullUrl.endsWith("/") || fullSuffix.startsWith("/"))
                separator = "";
            else
                separator = "/";

            fullUrl += separator + fullSuffix;
        }

        if (isWithParams)
            fullUrl += mapToUrlQuery(params);

        return fullUrl;
    }

    private String mapToUrlQuery(Map<String, Object> paramsHash) {
        if (TextUtils.isEmpty(baseUrl)) {
            return null;
        }

        StringBuilder queryBuilder = new StringBuilder("");

        if (paramsHash != null && paramsHash.size() > 0) {
            if (!baseUrl.contains("?")) {
                queryBuilder.append("?");
            }

            for (String key : paramsHash.keySet()) {
                try {
                    String val = URLEncoder.encode(paramsHash.get(key)
                            .toString(), "UTF-8");
                    queryBuilder.append(key).append("=").append(val)
                            .append("&");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            queryBuilder.deleteCharAt(queryBuilder.length() - 1);
        }

        return queryBuilder.toString();
    }

    private void checkHeaders(AjaxCallback<?> callback) {
        if (!headers.isEmpty()) {
            callbackString.url(baseUrl).type(String.class).timeout(timeout);

            for (String key : headers.keySet()) {
                callback.header(key, headers.get(key).toString());
            }
        }
    }

    protected void processResult(String result, int statusCode) {
        // Log.i(TAG, "code " + statusCode);
        // Log.i(TAG, "" + result);

        if (result == null) onCallBack(null, statusCode);
        else try {
            T response;
            response = new Parser<T>(type).parseFromJson(result);

            onCallBack(response, statusCode);

        } catch (Exception e) {
            e.printStackTrace();
            onCallBack(null, statusCode);
        }
    }

    protected void onCallBack(T obj, int statusCode) {
        if (getOnCallBackListener() != null)
            getOnCallBackListener().onCallBack(obj, statusCode);
    }

    public enum RequestType {
        GET, POST, PUT, DELETE
    }

    public interface OnCallBackListener<T> {
        public void onCallBack(T obj, int statusCode);
    }
}
