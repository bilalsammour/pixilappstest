package com.pixilapps.pixilappstest.models.thin;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ItemModel implements Serializable {

    private static final String UNDEFINED_CHAR = "u";
    private static final String WORLD_CHAR = "w";
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public ItemType getType() {
        if (description == null) return ItemType.UNDEFINED;

        String lowerDescription = description.toLowerCase();

        if (lowerDescription.startsWith(UNDEFINED_CHAR))
            return ItemType.UMBRELLA;
        else if (lowerDescription.startsWith(WORLD_CHAR))
            return ItemType.WORLD;
        else
            return ItemType.UNDEFINED;
    }

    public enum ItemType {UNDEFINED, UMBRELLA, WORLD}
}
