package com.pixilapps.pixilappstest.models.modelutil;

import android.content.Context;
import android.util.Base64;

import com.pixilapps.pixilappstest.R;
import com.pixilapps.pixilappstest.utils.Requester;

import java.io.UnsupportedEncodingException;

public class RequesterHelper {

    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";
    private static final String HEADER_AUTHORIZATION = "Authorization";

    public static void setBaseUrl(Context context, Requester<?> requester) {
        String baseUrl = context.getString(R.string.base_url);
        requester.setBaseUrl(baseUrl);
    }

    public static void addBasicAuthentication(Requester<?> requester) {
        String auth = getAuth();

        requester.addHeader(HEADER_AUTHORIZATION, auth);
    }

    private static String getAuth() {
        String userInfo = USERNAME + ":" + PASSWORD;
        String base64 = encode64(userInfo);
        String auth = "Basic " + base64;

        return auth;
    }

    private static String encode64(String text) {
        byte[] data = null;

        try {
            data = text.getBytes("UTF-8");
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }

        String base64 = Base64.encodeToString(data, Base64.DEFAULT);

        return base64;
    }
}
