package com.pixilapps.pixilappstest.models.loaders;

import android.content.Context;

import com.pixilapps.pixilappstest.models.modelutil.RequesterHelper;
import com.pixilapps.pixilappstest.utils.Requester;

public abstract class BaseLoader<T> {

    protected static final int DEFAULT_LAST_ID = 0;
    protected static final String PARAM_LAST_ID = "last_id";
    protected static final String PARAM_DOCTOR_NAME = "doctor_name";
    protected static final String PARAM_PATIENT_NAME = "patient_name";
    protected Context context;
    protected Class<T> classType;
    protected T dataSource;
    protected Requester<T> requester;
    protected boolean append;
    protected Requester.OnCallBackListener<T> callBackListener = new Requester.OnCallBackListener<T>() {
        @Override
        public void onCallBack(T obj, int statusCode) {
            onDataSourceCallBack(obj, statusCode);
        }
    };
    private OnRetrievedListener onRetrievedListener;

    public BaseLoader(Context context, Class<T> classType) {
        this.context = context;
        this.classType = classType;

        initRequester();
    }

    protected void initRequester() {
        this.requester = new Requester<>(context, classType);
        RequesterHelper.setBaseUrl(context, requester);
        RequesterHelper.addBasicAuthentication(requester);

        requester.setOnCallBackListener(callBackListener);
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public T getDataSource() {
        return dataSource;
    }

    public void setDataSource(T dataSource) {
        this.dataSource = dataSource;
    }

    public OnRetrievedListener getOnRetrievedListener() {
        return onRetrievedListener;
    }

    public void setOnRetrievedListener(OnRetrievedListener onRetrievedListener) {
        this.onRetrievedListener = onRetrievedListener;
    }

    protected abstract void onDataSourceCallBack(T obj, int statusCode);

    public interface OnRetrievedListener<T> {
        public void onRetrieved(T obj);
    }
}
