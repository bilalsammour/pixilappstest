package com.pixilapps.pixilappstest.models.loaders;

import android.content.Context;

import com.pixilapps.pixilappstest.models.thin.ItemModel;
import com.pixilapps.pixilappstest.utils.Requester;

public class ItemsLoader extends BaseLoader<ItemModel[]> {

    public ItemsLoader(Context context) {
        super(context, ItemModel[].class);
        setDataSource(null);
    }

    public void retrieve() {
        renewRequester();

        requester.makeRequest(Requester.RequestType.GET);
    }

    private void renewRequester() {
        this.append = false;
        initRequester();
    }

    @Override
    protected void onDataSourceCallBack(ItemModel[] obj, int statusCode) {
        if (obj != null) {
            dataSource = obj;

            if (getOnRetrievedListener() != null)
                getOnRetrievedListener().onRetrieved(dataSource);
        }
    }
}
